#include <iostream>
using namespace std;

class Person {
private:
    char *name;
    int age;
    bool gender;
public:
    Person() {
        name = NULL;
        age = 0;
        gender = 0;
        cout << "Конструктор без параметров - " << this << endl;
    }
    Person(char *name, int age, bool gender) {
        this->name = new char[strlen(name)];
        // this->name = name;
        strcpy(this->name, name);
        this->age = age;
        this->gender = gender;
        cout << "Конструктор с параметром - " << this << endl;
    }
    Person(Person & value){
        name = new char[strlen(value.getName())];
        name = value.getName();
        age = value.getAge();
        gender = value.getGender();
        cout << "Конструктор копирования - " << this << endl;
    }
    ~Person() {
        if (name) {
            delete[] name;
            cout << "Диструктор - " << this << endl;
        }
    }
    char* getName();
    int getAge();
    bool getGender();
    void setName(char *newName);
    void setAge(int age);
    void setGender(bool gender);
    void print();
};

int main() {
    Person empty;
    Person frank("Frank", 23, true);
    frank.print();
    Person Copy(frank);
    Copy.print();
    return 0;
}

char* Person::getName() {
    return name;
}

int Person::getAge() {
    return age;
}

bool Person::getGender() {
    return gender;
}

void Person::setName(char *newName) {
    name = new char[strlen(newName)];
    name = newName;
}

void Person::setAge(int age) {
    this->age = age;
}

void Person::setGender(bool gender) {
    this->gender = gender;
}

void Person::print() {
    cout << "Name   : " << name << endl;
    cout << "Age    : " << age  << endl;
    if (gender) {
        cout << "Gender : male" << endl;
    }
    else {
        cout << "Gender : female" << endl;
    }
}
