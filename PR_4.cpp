// Практическая работа 4
// программа переводит сумму из новой английской системы в старую
#include <iostream>
#include <cmath>
using namespace std;
// структура для работы с данными
struct sterling {
  int pounds, shillings, pence;
};
// переводит в формат старую английскую систему
sterling oldMoney(double pounds) {
  int a, c;
  a = pounds; // отделяет фунты
  c = (pounds - a) * 100; // отделяет дробную часть
  cout << c << endl << a << endl;
  sterling str;
  c += a * 240; // переводит в пенсы
  // отделяет фунты, шилинги и пенсы
  str.pounds = c / 240;
  c %= 240;
  str.shillings = c / 12;
  str.pence = c % 12;

  return str;
}

int main() {
  double pounds;
  cout << "Input money in new system: ";
  cin  >> pounds;
  sterling str = oldMoney(pounds);
  cout  << "In old system: "
        << str.pounds    << "."
        << str.shillings << "."
        << str.pence     << endl;
  return 0;
}
