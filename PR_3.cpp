// Практи работа 3
// Программа складывает 2 времени в 12-ти часовом формате
#include<iostream>
#include <iomanip>
using namespace std;
// структура с данными о времени
struct tim {
	int hours, minutes, seconds;
	bool checkTime();
	int second();
};
// проверка правильности ввода данных
bool tim::checkTime() {
	return (hours >= 0 && hours < 13) && (minutes >= 0 && minutes < 60) && (seconds >= 0 && seconds < 60);
}
// переводит время в секунды
int tim::second() {
	return (hours * 60) * 60 + minutes * 60 + seconds;
}
// считает новое время из суммы секунд введенных данных
void inTime(int second) {
	tim newTime;
	newTime.hours		= (second / 60) / 60;
	newTime.minutes	= (second / 60 ) % 60;
	newTime.seconds	= (second % 60 ) % 60;
	if (newTime.hours >= 12)
		newTime.hours = newTime.hours % 12;

	cout << "new time: " << setfill('0') << setw(2) << newTime.hours << ':' << setw(2) << newTime.minutes << ':' << setw(2) << newTime.seconds << endl;
}
// основная функция
int main() {
	tim tmOne;
	tim tmTwo;

	cout	<< "Input first time (HH:MM:SS): ";
	cin		>> tmOne.hours >> tmOne.minutes >> tmOne.seconds;
	cout 	<< "Input second time (HH:MM:SS): ";
	cin 	>> tmTwo.hours >> tmTwo.minutes >> tmTwo.seconds;
	// проверка ввода данных
	if (tmOne.checkTime() && tmTwo.checkTime()) {
		int allSeconds = tmOne.second() + tmTwo.second();
		cout << "All seconds: " << allSeconds << endl;
		inTime(allSeconds);
	}	else {
		cout << "Error input!\n";
	}
	return 0;
}
