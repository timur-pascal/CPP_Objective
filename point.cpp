#ifndef class_point
#define class_point

class point {
private:
    double x;
    double y;
public:
    point(double initial_x = 0.0, double initial_y = 0.0);

    void shift(double x_amount, double y_amount);
    void rotate90();

    double get_x() const {
        return x;
    }
    double get_y() const {
        return y;
    }
};
#endif
